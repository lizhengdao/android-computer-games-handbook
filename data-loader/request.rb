require 'logger'
require 'tempfile'
require 'uri'
require 'json'
require 'google/cloud/storage'
require 'google/cloud/firestore'


API_URL = 'https://api.rawg.io/api'
RAWG_PAGE_SIZE = 20
LOAD_COUNT = 50

PROJECT_ID = 'computer-game-handbook'
FIREBASE_URL = "https://#{PROJECT_ID}.firebaseio.com"
ADMINSDK_KEY_PATH = "./#{PROJECT_ID}-adminsdk.json"
GC_BUCKET_NAME = "#{PROJECT_ID}.appspot.com"

DATA_DIR = File.join(__dir__, '../Data')
JSON_PATH = File.join(DATA_DIR, 'games.json')

MEDIA_RELATIVE_DIR = 'media'
IMAGE_RESOURCE = 'image'
CLIP_RESOURCE = 'clip'


# Logger
$logger = Logger.new(STDOUT)
$logger.level = Logger::INFO

# Google
Google::Apis.logger = $logger

$firestore = Google::Cloud::Firestore.new(
  project_id: PROJECT_ID,
  credentials: ADMINSDK_KEY_PATH,
)

$storage = Google::Cloud::Storage.new(
  project_id: PROJECT_ID,
  credentials: ADMINSDK_KEY_PATH,
)
$bucket = $storage.bucket GC_BUCKET_NAME


$games_ref = $firestore.col 'games'
$tags_ref = $firestore.col 'tags'
$genres_ref = $firestore.col 'genres'
$platforms_ref = $firestore.col 'platforms'



def rawg_query(endpoint, query_params = {})
  uri = URI.parse("#{API_URL}#{endpoint}")
  uri.query = URI.encode_www_form(query_params)
  headers = { 'Accept' => '*/*' }

  begin
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    $logger.info("API request to '#{uri}'")
    response = http.request_get("#{uri.path}?#{uri.query}", headers)

    if response.code != '200'
      $logger.error("API HTTP error: #{response.code}")
      throw "API HTTP error: #{response.code}"
    end
  rescue => error
    $logger.fatal("API request error for '#{uri}': #{error}")
    throw "API request error for '#{uri}': #{error}"
  end
  JSON.parse(response.body)
end

def rawg_load_top_games(count)
  games = []
  page = 1
  while games.length < count do
    now = Date.today
    from = (now - 365 * 15).strftime('%F')
    to = (now + 30).strftime('%F')
    query_games = rawg_query('/games', { 
      page: page, 
      page_size: RAWG_PAGE_SIZE,
      ordering: '-rating',
      dates: "#{from},#{to}" 
    })
    query_games = query_games['results'].take([count - games.length, RAWG_PAGE_SIZE].min)
    games.push(*query_games)
    page += 1
  end
  games
end

def donwload_rawg_media_resource(resource_url)
  return nil if resource_url.nil?

  resource_id = resource_url[/\/([^\/]+)\.[^.]+$/, 1]
  resource_extension = resource_url[/\.([^.]+)$/, 1]

  uri = URI.parse(resource_url)
  headers = { 'Accept' => '*/*' }

  begin
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    file = Tempfile.new([resource_id, resource_extension])

    $logger.info("Loading resource using API request to '#{resource_url}' to path '#{file.path}'")

    http.request_get(uri.path, headers) do |response|
      file.write(response.body)
    end

    ["#{resource_id}.#{resource_extension}", file]
  rescue => error
    file.close if file != nil
    $logger.fatal("Media loading error for resource '#{uri}': #{error}")
    throw "Media loading error for resource '#{uri}': #{error}"
  end
end

def upload_resource(resource_type, resource_url)
  return nil if resource_url.nil?
  
  temp_file_name, temp_file = donwload_rawg_media_resource(resource_url)

  relative_path = File.join(resource_type, temp_file_name[0..2], temp_file_name)

  gc_file = $bucket.create_file temp_file, relative_path

  $logger.info("Uploaded '#{resource_url}' to '#{gc_file.public_url}'")
  temp_file.close

  gc_file.public_url
end

def upload_firestore_object(resource_name, collection_reference, object)
  doc_ref = collection_reference.add object
  if !doc_ref
    $logger.error("Resource '#{resource_name}' upload error")
    throw "Resource '#{resource_name}' not uploaded #{object}"
  end

  $logger.info("Uploaded '#{resource_name}' to '#{doc_ref}': #{object}")
  doc_ref
end

def download_firestore_object(resource_name, collection_reference, field, value)
  object = nil
  collection_reference.
    where(field, '=', value).
    limit(1).
    get do |found_object|
      object = found_object
    end

  $logger.info("Downloaded '#{resource_name}' from '#{field}:#{value}': #{object&.data}")
  if object == nil
    nil
  else
    [object.ref, object.data]
  end
end

def upload_platforms(platforms)
  platform_names = platforms.map { |platform| platform['platform']['name'] }
  platform_names.map do |platform_name|
    imported_platform = {
      name: platform_name,
    }

    platform = download_firestore_object('platform', $platforms_ref, 'name', platform_name)
    
    platform&.[](0) || upload_firestore_object('platform', $platforms_ref, imported_platform)
  end
end

def upload_tags(tags)
  tag_names = tags.select { |tag| tag['language'] == 'eng' }.map { |tag| tag['name'] }
  tag_names.map do |tag_name|
    imported_tag = {
      name: tag_name,
    }

    tag = download_firestore_object('tag', $tags_ref, 'name', tag_name)
    
    tag&.[](0) || upload_firestore_object('tag', $tags_ref, imported_tag)
  end
end

def upload_genres(genres)
  genre_names = genres.map { |genre| genre['name'] }
  genre_names.map do |genre_name|
    imported_genre = {
      name: genre_name,
    }

    genre = download_firestore_object('genre', $genres_ref, 'name', genre_name)
    
    genre&.[](0) || upload_firestore_object('genre', $genres_ref, imported_genre)
  end
end

def upload_games(games)
  games.each do |game|
    extended_game = rawg_query("/games/#{game['id']}")

    image_src = extended_game['background_image']
    image_src = upload_resource(IMAGE_RESOURCE, image_src)

    clip_src = extended_game['clip']&.[]('clip')
    clip_src = upload_resource(CLIP_RESOURCE, clip_src)
    
    tags = upload_tags(game['tags'])
    genres = upload_genres(game['genres'])
    platforms = upload_platforms(game['platforms'])

    imported_game = {
      name: game['name'],
      released: game['released'],
      rating: game['rating'],
      added: game['added'],
      platforms: platforms,
      tags: tags,
      genres: genres,
      description: extended_game['description_raw'],
      website: extended_game['website'],
      image: image_src,
      clip: clip_src,
    }

    upload_firestore_object('game', $games_ref, imported_game)
  end
end

def load_games(count)
  games = rawg_load_top_games(count)
  upload_games(games)
end

load_games(LOAD_COUNT)

