package com.gitlab.valerykameko.computergameshandbook.data.model

import java.util.*

enum class Language(
    val locale: Locale,
) {
    ENGLISH(Locale("en")),
    RUSSIAN(Locale("ru"));

    val languageName
        get() = locale.displayLanguage

    val language
        get() = locale.language

    override fun toString() = languageName

    companion object {
        fun fromLanguage(language: String): Language? =
            values().find { it.language == language }
    }
}