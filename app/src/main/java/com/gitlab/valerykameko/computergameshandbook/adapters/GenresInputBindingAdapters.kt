package com.gitlab.valerykameko.computergameshandbook.adapters

import androidx.databinding.*
import com.gitlab.valerykameko.computergameshandbook.data.model.Genre
import com.gitlab.valerykameko.computergameshandbook.views.AutocompleteChipInput
import com.gitlab.valerykameko.computergameshandbook.views.GenresChipInput

@InverseBindingMethods(
    InverseBindingMethod(type = GenresChipInput::class, attribute = "genres"),
)
object GenresInputBindingAdapters {
    @JvmStatic
    @BindingAdapter("genres")
    fun AutocompleteChipInput.bindGenres(genres: List<Genre>?) =
        setValues(genres?.map(Genre::name))

    @JvmStatic
    @InverseBindingAdapter(attribute = "genres")
    fun AutocompleteChipInput.inverseBindGenres(): List<Genre> =
        getValues().map(::Genre)

    @JvmStatic
    @BindingAdapter("genresAttrChanged")
    fun AutocompleteChipInput.bindGenresListener(inverseBindingListener: InverseBindingListener?) =
        setListener { _, _ -> inverseBindingListener?.onChange() }

    @JvmStatic
    @BindingAdapter("autocompleteGenres")
    fun AutocompleteChipInput.bindAutocompleteGenres(autocompleteGenres: List<Genre>?) =
        setAutocompleteTags(autocompleteGenres?.map(Genre::name))
}