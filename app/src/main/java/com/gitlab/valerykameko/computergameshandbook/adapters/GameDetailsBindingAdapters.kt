package com.gitlab.valerykameko.computergameshandbook.adapters

import androidx.databinding.BindingAdapter
import com.gitlab.valerykameko.computergameshandbook.data.model.Genre
import com.gitlab.valerykameko.computergameshandbook.data.model.Platform
import com.gitlab.valerykameko.computergameshandbook.data.model.Tag
import com.gitlab.valerykameko.computergameshandbook.views.GenreChip
import com.gitlab.valerykameko.computergameshandbook.views.PlatformChip
import com.gitlab.valerykameko.computergameshandbook.views.TagChip
import com.google.android.material.chip.ChipGroup

object GameDetailsBindingAdapters {
    @JvmStatic
    @BindingAdapter("genres")
    fun bindGenres(chipGroup: ChipGroup, genres: List<Genre>?) {
        genres?.onEach { genre ->
            val chip = GenreChip(chipGroup.context, genre)
            chipGroup.addView(chip)
        }
    }

    @JvmStatic
    @BindingAdapter("tags")
    fun bindTags(chipGroup: ChipGroup, genres: List<Tag>?) {
        genres?.onEach { tag ->
            val chip = TagChip(chipGroup.context, tag)
            chipGroup.addView(chip)
        }
    }

    @JvmStatic
    @BindingAdapter("platforms")
    fun bindPlatforms(chipGroup: ChipGroup, genres: List<Platform>?) {
        genres?.onEach { platform ->
            val chip = PlatformChip(chipGroup.context, platform)
            chipGroup.addView(chip)
        }
    }
}
