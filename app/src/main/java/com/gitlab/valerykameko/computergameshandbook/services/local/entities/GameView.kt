package com.gitlab.valerykameko.computergameshandbook.services.local.entities

import android.net.Uri
import com.gitlab.valerykameko.computergameshandbook.data.model.Game
import com.google.gson.annotations.SerializedName
import java.util.*

data class GameView(
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("name")
    val name: String,
    @field:SerializedName("rating")
    val rating: Double,
    @field:SerializedName("released")
    val released: Date,
    @field:SerializedName("added")
    val added: Long,
    @field:SerializedName("platforms")
    val platforms: List<PlatformView>,
    @field:SerializedName("tags")
    val tags: List<TagView>,
    @field:SerializedName("genres")
    val genres: List<GenreView>,
    @field:SerializedName("description")
    val description: String,
    @field:SerializedName("website")
    val websiteSrc: Uri?,
    @field:SerializedName("image")
    val imageSrc: Uri?,
    @field:SerializedName("clip")
    val clipSrc: Uri?,
) {
    companion object {
        fun fromView(view: GameView) = with(view) {
            Game(
                id = id,
                name = name,
                rating = rating,
                releasedDate = released,
                addedCount = added,
                platforms = platforms.map(PlatformView.Companion::fromView),
                tags = tags.map(TagView.Companion::fromView),
                genres = genres.map(GenreView.Companion::fromView),
                description = description,
                websiteUri = websiteSrc,
                imageUri = imageSrc,
                clipUri = clipSrc
            )
        }

        fun toView(game: Game) = with(game) {
            GameView(
                id = id,
                name = name,
                rating = rating,
                released = releasedDate,
                added = addedCount,
                platforms = platforms.map(PlatformView.Companion::toView),
                tags = tags.map(TagView.Companion::toView),
                genres = genres.map(GenreView.Companion::toView),
                description = description,
                websiteSrc = websiteUri,
                imageSrc = imageUri,
                clipSrc = clipUri
            )
        }
    }
}