package com.gitlab.valerykameko.computergameshandbook.services.firebase.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.Tag

data class TagView(
    val value: String,
) {
    companion object {
        fun fromView(view: TagView) = Tag(view.value)
        fun toView(tag: Tag) = TagView(tag.value)
    }
}

