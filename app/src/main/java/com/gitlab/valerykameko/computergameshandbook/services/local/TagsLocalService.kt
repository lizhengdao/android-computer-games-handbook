package com.gitlab.valerykameko.computergameshandbook.services.local

import androidx.paging.ExperimentalPagingApi
import com.gitlab.valerykameko.computergameshandbook.services.local.entities.Chunk
import com.gitlab.valerykameko.computergameshandbook.services.local.entities.TagView
import com.gitlab.valerykameko.computergameshandbook.services.local.entities.TagsQueryView
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

@ExperimentalCoroutinesApi
class TagsLocalService(
    private val gamesService: GamesLocalService
) {
    private val tags: Flow<MutableSet<TagView>> = flow {
        val tags = mutableSetOf<TagView>()
        gamesService.getGames().collect { chunk ->
            chunk.items.asSequence()
                .flatMap { it.tags }
                .toCollection(tags)
        }
        emit(tags)
    }

    fun getTags(query: TagsQueryView?): Flow<Chunk<TagView>> = tags.map { tags ->
        val tagsSequence = tags.asSequence()
        val filteredTags = query?.let { query ->
            tagsSequence.filter(query::match)
        } ?: tagsSequence

        val count = query?.count ?: filteredTags.count()
        val tagsPrefix = filteredTags
            .take(count)
            .toList()
        Chunk(
            count = tagsPrefix.size,
            items = tagsPrefix,
        )
    }
}