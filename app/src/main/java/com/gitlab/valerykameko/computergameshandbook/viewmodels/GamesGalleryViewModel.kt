package com.gitlab.valerykameko.computergameshandbook.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagingData
import com.gitlab.valerykameko.computergameshandbook.data.GamesQueryStorage
import com.gitlab.valerykameko.computergameshandbook.data.GamesStorage
import com.gitlab.valerykameko.computergameshandbook.data.model.Game
import kotlinx.coroutines.flow.Flow
import timber.log.Timber

class GamesGalleryViewModel(
    private val gamesStorage: GamesStorage,
    gamesQueryStorage: GamesQueryStorage,
) : ViewModel() {
    private var games: Flow<PagingData<Game>>? = null
    private val query = gamesQueryStorage.query

    fun loadGames(): Flow<PagingData<Game>> {
        Timber.d("Reloading paged games")
        val games = gamesStorage.getGamesPaged(query.value)
        this.games = games
        return games
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(
        private val gamesStorage: GamesStorage,
        private val gamesQueryStorage: GamesQueryStorage,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            GamesGalleryViewModel(gamesStorage, gamesQueryStorage) as T
    }
}