package com.gitlab.valerykameko.computergameshandbook.adapters

import android.graphics.Typeface
import android.view.View
import androidx.core.view.children
import com.gitlab.valerykameko.computergameshandbook.util.tryOptional
import com.google.android.material.chip.ChipGroup

fun ChipGroup.setTypeface(typeface: Typeface) {
    this.children.forEach { view ->
        view.setCustomTypeface(typeface)
    }
}

fun View.setCustomTypeface(typeface: Typeface) {
    val method = tryOptional {
        this.javaClass.getMethod("setTypeface", Typeface::class.java)
    }
    method?.invoke(this, typeface)
}
