package com.gitlab.valerykameko.computergameshandbook.data

import android.content.Context
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.gitlab.valerykameko.computergameshandbook.data.model.Game
import com.gitlab.valerykameko.computergameshandbook.data.model.GameQuery
import com.gitlab.valerykameko.computergameshandbook.services.FirebaseServiceFactory
import com.gitlab.valerykameko.computergameshandbook.services.LocalServiceFactory
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.Cursor
import com.google.firebase.firestore.DocumentSnapshot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.GameView as GameFirebaseView
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.GamesQueryView as GamesQueryFirebaseView
import com.gitlab.valerykameko.computergameshandbook.services.local.entities.GameView as GameLocalView

class GamesStorage(
    context: Context
) {
    companion object {
        private const val PAGE_SIZE = 10
    }

    private val gamesFirebaseService by lazy { FirebaseServiceFactory.getGamesFirebaseService(context) }
    private val gamesLocalService by lazy { LocalServiceFactory.getGamesLocalService(context) }

    fun getGames(query: GameQuery?): Flow<List<Game>> =
        gamesFirebaseService.getGames(
            query = query?.let(::GamesQueryFirebaseView),
        ).map {
            it.items.map(GameFirebaseView.Companion::fromView)
        }

    internal fun getGamesPage(
        query: GameQuery? = null,
        key: DocumentSnapshot? = null,
        count: Long? = null,
    ): Flow<Cursor<Game>> =
        gamesFirebaseService.getGames(
            query = query?.let(::GamesQueryFirebaseView),
            cursor = key,
            count = count,
        ).map {
            Cursor(
                items = it.items.map(GameFirebaseView.Companion::fromView),
                nextKey = it.nextKey,
            )
        }

    fun getGamesPaged(query: GameQuery?) = Pager(
        config = PagingConfig(enablePlaceholders = false, pageSize = PAGE_SIZE),
        pagingSourceFactory = { GamesPagingSource(this, query) },
    ).flow

    // TODO: Add local caching
    fun getGame(id: String): Flow<Game?> =
        gamesFirebaseService.getGame(id).map { gameView ->
            gameView?.let(GameFirebaseView.Companion::fromView)
        }

    @ExperimentalCoroutinesApi
    suspend fun addGame(game: Game): String {
        val id = gamesFirebaseService.addGame(GameFirebaseView.toView(game)).id
        gamesLocalService.addGame(GameLocalView.toView(game.copy(id = id)))
        return id
    }

    @ExperimentalCoroutinesApi
    suspend fun removeGame(id: String) {
        gamesLocalService.removeGame(id)
        gamesFirebaseService.removeGame(id)
    }
}