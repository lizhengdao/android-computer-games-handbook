package com.gitlab.valerykameko.computergameshandbook.data

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.gitlab.valerykameko.computergameshandbook.services.FirebaseServiceFactory
import com.gitlab.valerykameko.computergameshandbook.services.LocalServiceFactory
import kotlinx.coroutines.Dispatchers
import timber.log.Timber

class MediaStorage(
    context: Context
) {
    private val firebaseService by lazy { FirebaseServiceFactory.getMediaFirebaseService(context) }
    private val localService by lazy { LocalServiceFactory.getMediaLocalService(context) }

    fun getImageUri(uri: Uri): LiveData<Uri?> = liveData(Dispatchers.IO) {
        emit(firebaseService.getImageUri(uri))
    }

    fun getClipUri(uri: Uri): LiveData<Uri?> = liveData(Dispatchers.IO) {
        emit(firebaseService.getClipUri(uri))
    }

    // TODO: Add local caching
    fun fetchImage(uri: Uri): LiveData<Bitmap?> = getImageData(uri)
        .map { imageData ->
            if (imageData == null) {
                null
            } else {
                BitmapFactory.decodeByteArray(imageData, 0, imageData.size)
            }
        }

    // TODO: Add local caching
    private fun getImageData(uri: Uri): LiveData<ByteArray?> = liveData(Dispatchers.IO) {
        Timber.d("We loading $uri!!!")
        emit(firebaseService.getImageData(uri))
    }

    // TODO: Add local caching
    private fun getClipData(uri: Uri): LiveData<ByteArray?> = liveData(Dispatchers.IO) {
        Timber.d("We loading $uri!!!")
        emit(firebaseService.getClipData(uri))
    }

    suspend fun addImage(data: ByteArray, extension: String): Uri {
        val uri = firebaseService.addImage(data, extension)
        localService.addImage(uri, data)
        return uri
    }

    suspend fun removeImage(uri: Uri) {
        firebaseService.removeImage(uri)
        localService.removeImage(uri)
    }

    fun addClip(data: ByteArray, extension: String): LiveData<Uri> = liveData(Dispatchers.IO) {
        val uri = firebaseService.addClip(data, extension)
        localService.addClip(uri, data)
        emit(uri)
    }

    suspend fun removeClip(uri: Uri) {
        firebaseService.removeClip(uri)
        localService.removeClip(uri)
    }
}
