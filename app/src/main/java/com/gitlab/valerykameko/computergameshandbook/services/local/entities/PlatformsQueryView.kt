package com.gitlab.valerykameko.computergameshandbook.services.local.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.PlatformQuery

data class PlatformsQueryView(
    val prefix: String,
    val count: Int,
    val ignoreCase: Boolean = true,
) {
    constructor(query: PlatformQuery) : this(
        prefix = query.prefix ?: "",
        count = query.count,
    )

    fun match(platformView: PlatformView) = when {
        !platformView.name.startsWith(prefix, ignoreCase) -> false
        else -> true
    }

}