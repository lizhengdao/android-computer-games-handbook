package com.gitlab.valerykameko.computergameshandbook.data

import android.content.Context
import com.gitlab.valerykameko.computergameshandbook.data.model.Tag
import com.gitlab.valerykameko.computergameshandbook.data.model.TagQuery
import com.gitlab.valerykameko.computergameshandbook.services.FirebaseServiceFactory
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.TagView
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.TagsQueryView
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber

class TagsStorage(
    context: Context,
) {
    companion object {
        private const val MAX_TAGS_COUNT_SIZE = 50
    }

    private val tagsFirebaseService by lazy { FirebaseServiceFactory.getTagsFirebaseService(context) }

    fun getTags(query: TagQuery?): Flow<List<Tag>> {
        Timber.d("Loading $query")
        return tagsFirebaseService.getTags(
            query = query?.copy(
                count = Integer.min(query.count, MAX_TAGS_COUNT_SIZE)
            )?.let(::TagsQueryView),
        ).map { items ->
            Timber.d("Loaded $items")
            items.map(TagView.Companion::fromView)
        }
    }
}

