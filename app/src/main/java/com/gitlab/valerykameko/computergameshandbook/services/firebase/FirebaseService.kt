package com.gitlab.valerykameko.computergameshandbook.services.firebase

import android.app.Activity
import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.tasks.await

class FirebaseService(context: Context) {
    private val firebaseFirestore: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val storage: FirebaseStorage = Firebase.storage

    var gamesService: GamesFirebaseService
    var mediaService: MediaFirebaseService
    var platformsService: PlatformsFirebaseService
    var tagsService: TagsFirebaseService
    var genresService: GenresFirebaseService

    init {
        platformsService = PlatformsFirebaseService(firebaseFirestore)
        tagsService = TagsFirebaseService(firebaseFirestore)
        genresService = GenresFirebaseService(firebaseFirestore)
        mediaService = MediaFirebaseService(storage.reference)
        gamesService = GamesFirebaseService(
            firebaseFirestore, genresService, platformsService, tagsService
        )
    }
}