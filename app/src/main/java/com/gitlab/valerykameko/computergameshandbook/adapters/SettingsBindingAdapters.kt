package com.gitlab.valerykameko.computergameshandbook.adapters

import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.data.model.GameField
import com.gitlab.valerykameko.computergameshandbook.data.model.SortOrder
import com.gitlab.valerykameko.computergameshandbook.services.entities.FontEntry

object SettingsBindingAdapters {
    @JvmStatic
    @BindingAdapter("fontEntry")
    fun TextView.bindFontEntry(fontEntry: FontEntry?) {
        fontEntry?.let {
            text = it.name
            typeface = it.typeface
        }
    }
}
