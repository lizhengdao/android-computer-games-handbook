package com.gitlab.valerykameko.computergameshandbook.services.firebase

import com.google.firebase.firestore.Query

fun Query.whereStartsWith(field: String, prefix: String): Query {
    if (prefix.isEmpty()) return this

    val lowerBound = prefix
    val length = prefix.length
    val endChar = prefix.toCharArray().last() + 1
    val upperBound = prefix.slice(0 until (length - 1)) + endChar

    return this
        .whereLessThanOrEqualTo(field, lowerBound)
        .whereLessThan(field, upperBound)
}