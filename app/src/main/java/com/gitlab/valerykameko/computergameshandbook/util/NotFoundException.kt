package com.gitlab.valerykameko.computergameshandbook.util

class NotFoundException(message: String? = null) : Exception(message)