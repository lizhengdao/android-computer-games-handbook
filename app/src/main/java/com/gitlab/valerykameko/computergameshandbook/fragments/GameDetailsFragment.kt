package com.gitlab.valerykameko.computergameshandbook.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.core.net.toFile
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.gitlab.valerykameko.computergameshandbook.BuildConfig
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.data.StorageFactory
import com.gitlab.valerykameko.computergameshandbook.databinding.FragmentGameDetailsBinding
import com.gitlab.valerykameko.computergameshandbook.util.Result
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GameDetailsViewModel
import com.gitlab.valerykameko.computergameshandbook.viewmodels.GlobalSettingsViewModel
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber

class GameDetailsFragment : Fragment() {
    companion object {
        private const val VIDEO_MIME_TYPE = "video/*"
    }

    private val args: GameDetailsFragmentArgs by navArgs()

    private lateinit var binding: FragmentGameDetailsBinding
    private val viewModel: GameDetailsViewModel by viewModels {
        GameDetailsViewModel.Factory(
            StorageFactory.getGamesStorage(requireContext()),
            StorageFactory.getMediaStorage(requireContext()),
            args.gameId
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = DataBindingUtil.inflate<FragmentGameDetailsBinding>(
            inflater, R.layout.fragment_game_details, container, false
        ).apply {
            lifecycleOwner = viewLifecycleOwner
            gameDetailsViewModel = viewModel
            globalSettingsViewModel = GlobalSettingsViewModel.getInstance(requireContext())

            viewModel.fetch()

            viewModel.clipUri.observe(viewLifecycleOwner) {}
            viewModel.websiteUri.observe(viewLifecycleOwner) {}

            openWebsiteButton.setOnClickListener {
                val websiteUri = viewModel.websiteUri.value
                websiteUri?.let { websiteUri ->
                    try {
                        startActivity(Intent(Intent.ACTION_VIEW, websiteUri))
                    } catch (ex: Exception) {
                        showError("Invalid URI")
                    }
                }
            }

            openVideoButton.setOnClickListener {
                viewModel.clipUri.value.let { clipUri ->
                    Timber.d("View clip is $clipUri")
                    if (clipUri == null) {
                        Snackbar
                            .make(requireView().rootView,
                                getString(R.string.game_details_no_video_error),
                                Snackbar.LENGTH_SHORT)
                            .show()
                    } else {
                        val exportUri = FileProvider.getUriForFile(requireContext(), "${BuildConfig.APPLICATION_ID}.provider", clipUri.toFile())
                        startActivity(
                            Intent()
                                .setAction(Intent.ACTION_VIEW)
                                .setDataAndType(exportUri, VIDEO_MIME_TYPE)
                                .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        )
                    }
                }
            }

            toolbar.setNavigationOnClickListener { view ->
                view.findNavController().navigateUp()
            }

            viewModel.gameResult.observe(viewLifecycleOwner) { result ->
                when (result) {
                    Result.Loading -> {
                        imageView.visibility = View.GONE
                        content.visibility = View.GONE
                        progressBar.visibility = View.VISIBLE
                    }
                    else -> {
                        imageView.visibility = View.VISIBLE
                        content.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                    }
                }
            }
        }
        return binding.root
    }

    private fun showError(error: String) {
        val snackbar = Snackbar.make(
            requireView().rootView, error, Snackbar.LENGTH_SHORT
        )
        snackbar.show()
    }
}