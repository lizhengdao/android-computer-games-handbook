package com.gitlab.valerykameko.computergameshandbook.data

import android.content.Context

internal class Storage(
    context: Context
) {
    val queryStorage = GamesQueryStorage()

    val settingsStorage = SettingsStorage(context)

    val platformsStorage = PlatformsStorage(context)
    val genresStorage = GenresStorage(context)
    val gamesStorage = GamesStorage(context)
    val mediaStorage = MediaStorage(context)
    val tagsStorage = TagsStorage(context)
}