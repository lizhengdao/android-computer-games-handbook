package com.gitlab.valerykameko.computergameshandbook.util

import com.gitlab.valerykameko.computergameshandbook.data.model.SortOrder

enum class SortingOrderView {
    ASC,
    DESC;

    companion object {
        fun toView(sortOrder: SortOrder) = when(sortOrder) {
            SortOrder.ASC -> ASC
            SortOrder.DESC -> DESC
        }
    }
}

abstract class FieldSortingView<out F, T>(
    protected val field: F,
    protected val order: SortingOrderView,
) {
    abstract val comparator : Comparator<T>
}

abstract class FieldsSortingView<out F, T>(
    protected val fieldsSorting: List<FieldSortingView<F, T>>
) {
    abstract val comparator : Comparator<T>
}