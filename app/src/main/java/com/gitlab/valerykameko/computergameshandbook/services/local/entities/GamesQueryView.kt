package com.gitlab.valerykameko.computergameshandbook.services.local.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.FieldsSorting
import com.gitlab.valerykameko.computergameshandbook.data.model.GameQuery

data class GamesQueryView(
    val nameSubstring: String?,
    val tags: List<TagView>?,
    val genres: List<GenreView>?,
    val platforms: List<PlatformView>?,
    val minAddedBy: Int?,
    val maxAddedBy: Int?,
    val minRating: Float?,
    val maxRating: Float?,
    val sorting: GameFieldsSortingView,
) {
    constructor(gameQuery: GameQuery) : this(
        nameSubstring = gameQuery.titleSubstring,
        tags = gameQuery.tags?.map(TagView.Companion::toView),
        genres = gameQuery.genres?.map(GenreView.Companion::toView),
        platforms = gameQuery.platforms?.map(PlatformView.Companion::toView),
        minAddedBy = gameQuery.minAddedBy,
        maxAddedBy = gameQuery.maxAddedBy,
        minRating = gameQuery.minRating,
        maxRating = gameQuery.maxRating,
        sorting = GameFieldsSortingView(
            FieldsSorting(gameQuery.sortingField)
        ),
    )

    val comparator : Comparator<GameView> by lazy(sorting::comparator)

    fun match(gameView: GameView) = when {
        minAddedBy != null && gameView.added < minAddedBy -> false
        maxAddedBy != null && gameView.added > maxAddedBy -> false
        minRating != null && gameView.rating < minRating -> false
        maxRating != null && gameView.rating > maxRating -> false
        tags != null && !gameView.tags.containsAll(tags) -> false
        genres != null && !gameView.genres.containsAll(genres) -> false
        platforms != null && !gameView.platforms.containsAll(platforms) -> false
        nameSubstring != null && nameSubstring.toLowerCase() !in gameView.name.toLowerCase() -> false
        else -> true
    }
}