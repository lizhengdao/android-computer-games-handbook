package com.gitlab.valerykameko.computergameshandbook.views

import android.content.Context
import android.util.AttributeSet
import com.gitlab.valerykameko.computergameshandbook.data.model.Platform
import com.gitlab.valerykameko.computergameshandbook.data.model.Tag
import com.gitlab.valerykameko.computergameshandbook.views.AutocompleteChipInput.ChipFactory

class PlatformsChipInput @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
): AutocompleteChipInput(context, attrs, defStyle) {
    override val chipFactory = ChipFactory { context, value -> PlatformChip(context, Platform(value)) }
}

