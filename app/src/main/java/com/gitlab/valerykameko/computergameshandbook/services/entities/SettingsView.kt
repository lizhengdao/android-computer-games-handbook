package com.gitlab.valerykameko.computergameshandbook.services.entities

import android.net.Uri
import com.google.gson.annotations.SerializedName

data class SettingsView(
    @field:SerializedName("font")
    val font: Uri?,
    @field:SerializedName("language")
    val language: String?,
)
