package com.gitlab.valerykameko.computergameshandbook.services.firebase.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.GenreQuery
import com.gitlab.valerykameko.computergameshandbook.services.firebase.whereStartsWith
import com.google.firebase.firestore.Query

data class GenresQueryView(
    val prefix: String,
    val count: Int,
    val ignoreCase: Boolean = true,
) {
    constructor(genreQuery: GenreQuery) : this(
        prefix = genreQuery.prefix ?: "",
        count = genreQuery.count,
    )

    fun applyQuery(query: Query): Query {
        return query
            .whereStartsWith("name", prefix)
            .limit(count.toLong())
    }
}