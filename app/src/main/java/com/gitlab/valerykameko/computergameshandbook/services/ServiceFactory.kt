package com.gitlab.valerykameko.computergameshandbook.services

import android.content.Context

object ServiceFactory {
    @Volatile
    private var fontsService: FontsService? = null
    @Volatile
    private var settingsService: SettingsService? = null

    fun getFontsService(context: Context) =
        fontsService ?: synchronized(this) {
            fontsService ?: FontsService(context).also { fontsService = it }
        }

    fun getSettingsService(context: Context) =
        settingsService ?: synchronized(this) {
            settingsService ?: SettingsService(context).also { settingsService = it }
        }
}