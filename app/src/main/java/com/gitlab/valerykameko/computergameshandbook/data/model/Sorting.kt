package com.gitlab.valerykameko.computergameshandbook.data.model

import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.util.Translatable

enum class SortOrder(
    override val translation: Int
) : Translatable {
    ASC(R.string.sorting_order_ascending),
    DESC(R.string.sorting_order_descending),
}

data class FieldSorting<F>(
    val field: F,
    val order: SortOrder,
)

data class FieldsSorting<F>(
    val fieldsSorting: List<FieldSorting<F>> = listOf()
) {
    constructor(fieldSorting: FieldSorting<F>? = null) : this(listOfNotNull(fieldSorting))
}