package com.gitlab.valerykameko.computergameshandbook.data

data class PageChunk<T>(
    val pages: Int,
    val items: List<T>,
)