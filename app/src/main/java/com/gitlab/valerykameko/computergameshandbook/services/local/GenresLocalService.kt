package com.gitlab.valerykameko.computergameshandbook.services.local

import com.gitlab.valerykameko.computergameshandbook.services.local.entities.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

@ExperimentalCoroutinesApi
class GenresLocalService(
    private val gamesService: GamesLocalService
) {
    private val genres: Flow<MutableSet<GenreView>> = flow {
        val genres = mutableSetOf<GenreView>()
        gamesService.getGames().collect { chunk ->
            chunk.items.asSequence()
                .flatMap { it.genres }
                .toCollection(genres)
        }
        emit(genres)
    }

    fun getGenres(query: GenresQueryView?): Flow<Chunk<GenreView>> = genres.map { tags ->
        val genresSequence = tags.asSequence()
        val filteredGenres = query?.let { query ->
            genresSequence.filter(query::match)
        } ?: genresSequence

        val count = query?.count ?: filteredGenres.count()
        val genresPrefix = filteredGenres
            .take(count)
            .toList()
        Chunk(
            count = genresPrefix.size,
            items = genresPrefix,
        )
    }
}