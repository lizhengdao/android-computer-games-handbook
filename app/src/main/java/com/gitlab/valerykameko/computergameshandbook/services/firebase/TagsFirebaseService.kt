package com.gitlab.valerykameko.computergameshandbook.services.firebase

import androidx.collection.LruCache
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.PlatformView
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.TagView
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.TagsQueryView
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await
import timber.log.Timber

class TagsFirebaseService(
    private val firebaseFirestore: FirebaseFirestore
) {
    private val tagsCollection = firebaseFirestore.collection("tags")
    private val cache: LruCache<String, TagView> = LruCache(30)

    fun getTags(query: TagsQueryView?): Flow<List<TagView>> = flow {
        var firebaseQuery: Query = tagsCollection

        query?.let {
            firebaseQuery = query.applyQuery(firebaseQuery)
        }

        val tagsSnapshot = firebaseQuery.get().await()
        val tagViews = tagsSnapshot.mapNotNull { deserializeTag(it) }

        emit(tagViews)
    }

    fun getTag(id: String): Flow<TagView?> = flow {
        val tagView = cache.get(id) ?: run {
            val tagDocument = tagsCollection.document(id)
            val tagSnapshot = tagDocument.get().await()
            val tagView = deserializeTag(tagSnapshot)

            Timber.i("Loaded tag $tagView")
            tagView?.also { cache.put(id, tagView) }
        }

        emit(tagView)
    }

    suspend fun addTag(tag: TagView): DocumentReference {
        val sameTagsQuery = tagsCollection.whereEqualTo("name", tag.value)
        val sameTagsSnapshot = sameTagsQuery.get().await()
        sameTagsSnapshot.forEach { sameTagSnapshot ->
            if (sameTagSnapshot.exists())
                return sameTagSnapshot.reference
        }

        val tagSnapshot = serializeTag(tag)
        return tagsCollection.add(tagSnapshot).await()
    }

    private fun deserializeTag(document: DocumentSnapshot): TagView? {
        if (!document.exists())
            return null
        return TagView(
            value = document.getString("name")!!
        )
    }

    private fun serializeTag(tag: TagView): Map<String, Any> {
        return mapOf(
            "name" to tag.value
        )
    }
}
