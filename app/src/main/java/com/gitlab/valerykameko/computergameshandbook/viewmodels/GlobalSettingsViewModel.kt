package com.gitlab.valerykameko.computergameshandbook.viewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.map
import com.gitlab.valerykameko.computergameshandbook.data.*

class GlobalSettingsViewModel private constructor(
    context: Context
) : ViewModel() {
    private val settingsStorage: SettingsStorage = StorageFactory.getSettingsStorage(context)
    private val settings = settingsStorage.getSettings()
    val font = settings.map { it.fontEntry.typeface }

    companion object {
        @Volatile
        private var instance: GlobalSettingsViewModel? = null

        fun getInstance(context: Context) =
            instance ?: synchronized(this) {
                instance ?: GlobalSettingsViewModel(context).also { instance = it }
            }
    }
}
