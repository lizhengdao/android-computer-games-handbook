package com.gitlab.valerykameko.computergameshandbook.data.model

data class TagQuery(
    val prefix: String? = null,
    val count: Int,
)