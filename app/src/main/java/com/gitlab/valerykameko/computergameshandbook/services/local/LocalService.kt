package com.gitlab.valerykameko.computergameshandbook.services.local

import android.content.Context
import android.content.res.AssetManager
import android.net.Uri
import androidx.core.net.toFile
import androidx.core.net.toUri
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber
import java.io.File

class LocalService internal constructor(context: Context) {
    companion object {
        private const val INITIAL_DATA_DIRECTORY = "Data"
        private const val GAMES_FILE = "games.json"
        private const val MEDIA_DIRECTORY = "media"
    }

    private val gamesFile = File(context.filesDir, GAMES_FILE)
    private val mediaFile = File(context.filesDir, MEDIA_DIRECTORY)
    @ExperimentalCoroutinesApi
    val gamesService: GamesLocalService = GamesLocalService(gamesFile)
    val mediaService: MediaLocalService =  MediaLocalService(mediaFile)
    @ExperimentalCoroutinesApi
    val tagsService: TagsLocalService = TagsLocalService(gamesService)
    @ExperimentalCoroutinesApi
    val genresService: GenresLocalService = GenresLocalService(gamesService)
    @ExperimentalCoroutinesApi
    val platformsService: PlatformsLocalService = PlatformsLocalService(gamesService)

    init {
        if (!isInitialized()) {
            initializeStorage(context)
        }
    }

    private fun isInitialized(): Boolean = gamesFile.exists()

    private fun initializeStorage(context: Context) =
        copyAsset(context.assets, Uri.parse(INITIAL_DATA_DIRECTORY), context.filesDir.toUri())

    private fun copyAsset(assetManager: AssetManager, from: Uri, to: Uri) {
        Timber.d("Copy asset $from to $to")
        val assetItems = assetManager.list(from.toString())
        if (assetItems == null || assetItems.isEmpty()) {
            assetManager.open(from.toString()).use { inStream ->
                val toFile = to.toFile()
                toFile.parentFile?.mkdirs()
                toFile.outputStream().use { outStream ->
                    inStream.copyTo(outStream)
                }
            }
        } else {
            assetItems.forEach { assetItem ->
                Timber.v("Call copyAsset $from to $to with $assetItem")
                copyAsset(assetManager,
                    Uri.withAppendedPath(from, assetItem),
                    Uri.withAppendedPath(to, assetItem))
            }
        }
    }
}