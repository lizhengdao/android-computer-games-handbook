package com.gitlab.valerykameko.computergameshandbook.services.firebase.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.GameField

enum class GameFieldView(
    val value: String?,
) {
    NONE(null),
    NAME("name"),
    RATING("rating"),
    ADDED("added");

    fun selectValue(gameView: GameView): Comparable<*> = when (this) {
        NONE -> NoneComparable
        NAME -> gameView.name
        RATING -> gameView.rating
        ADDED -> gameView.added
    }

    companion object {
        private object NoneComparable : Comparable<NoneComparable> {
            override fun compareTo(other: NoneComparable): Int = 0
        }

        fun toView(gameField: GameField? = null) = when (gameField) {
            GameField.TITLE -> NAME
            GameField.RATING -> RATING
            GameField.ADDED_BY -> ADDED
            else -> NONE
        }
    }
}
