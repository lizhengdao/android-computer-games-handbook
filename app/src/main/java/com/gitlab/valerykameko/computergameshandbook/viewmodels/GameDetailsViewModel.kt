package com.gitlab.valerykameko.computergameshandbook.viewmodels

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.lifecycle.*
import com.gitlab.valerykameko.computergameshandbook.data.GamesStorage
import com.gitlab.valerykameko.computergameshandbook.data.MediaStorage
import com.gitlab.valerykameko.computergameshandbook.data.model.Game
import com.gitlab.valerykameko.computergameshandbook.util.NotFoundException
import com.gitlab.valerykameko.computergameshandbook.util.Result
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class GameDetailsViewModel(
    private val gamesStorage: GamesStorage,
    private val mediaStorage: MediaStorage,
    private val id: String,
) : ViewModel() {
    private val _gameResult = MutableLiveData<Result<Game>>(Result.Loading)
    private val _game = MutableLiveData<Game>()
    val gameResult: LiveData<Result<Game>> = _gameResult
    val game: LiveData<Game> = _game

    val title = _game.map { it.name }
    val rating = _game.map { it.rating.toFloat() }
    val releasedDate = _game.map { RELEASED_DATE_FORMAT.format(it.releasedDate) }
    val addedCount = _game.map { it.addedCount }
    val platforms = _game.map { it.platforms }
    val tags = _game.map { it.tags }
    val genres = _game.map { it.genres }
    val description = _game.map { it.description }
    val bitmapUri = _game.map { it.imageUri }.switchMap { imageUri ->
        imageUri?.let(mediaStorage::getImageUri) ?: MutableLiveData(null)
    }
    val bitmap = _game.map { it.imageUri }.switchMap { imageUri ->
        imageUri?.let(mediaStorage::fetchImage) ?: MutableLiveData(null)
    }
    val websiteUri = _game.map { it.websiteUri }
    val clipUri = _game.map { it.clipUri }.switchMap { clipUri ->
        Timber.d("Clip is $clipUri")
        clipUri?.let(mediaStorage::getClipUri) ?: MutableLiveData(null)
    }

    val hasWebsiteUri = _game.map { it.websiteUri != null }

    fun fetch() {
        viewModelScope.launch {
            gamesStorage.getGame(id)
                .catch { ex -> _gameResult.postValue(Result.Error(ex)) }
                .collectLatest { game ->
                    if (game != null) {
                        _gameResult.postValue(Result.Success(game))
                        _game.postValue(game)
                    } else {
                        _gameResult.postValue(Result.Error(NotFoundException()))
                    }
                }
        }
    }

    companion object {
        private val RELEASED_DATE_FORMAT = SimpleDateFormat("MMM d, yyyy", Locale.US)
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(
        private val gamesStorage: GamesStorage,
        private val mediaStorage: MediaStorage,
        private val id: String,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            GameDetailsViewModel(gamesStorage, mediaStorage, id) as T
    }
}