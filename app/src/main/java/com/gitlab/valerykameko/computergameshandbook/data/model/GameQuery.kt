package com.gitlab.valerykameko.computergameshandbook.data.model

data class GameQuery(
    val titleSubstring: String?,
    val tags: List<Tag>?,
    val genres: List<Genre>?,
    val platforms: List<Platform>?,
    val minAddedBy: Int?,
    val maxAddedBy: Int?,
    val minRating: Float?,
    val maxRating: Float?,
    val sortingField: FieldSorting<GameField>?,
)