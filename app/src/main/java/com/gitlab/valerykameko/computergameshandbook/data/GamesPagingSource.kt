package com.gitlab.valerykameko.computergameshandbook.data

import androidx.paging.PagingSource
import com.gitlab.valerykameko.computergameshandbook.data.model.Game
import com.gitlab.valerykameko.computergameshandbook.data.model.GameQuery
import com.google.firebase.firestore.DocumentSnapshot
import kotlinx.coroutines.flow.first
import timber.log.Timber

internal class GamesPagingSource(
    private val storage: GamesStorage,
    private val query: GameQuery? = null,
) : PagingSource<DocumentSnapshot, Game>() {
    override suspend fun load(params: LoadParams<DocumentSnapshot>): LoadResult<DocumentSnapshot, Game> {
        return try {
            val page = params.key
            val perPage = params.loadSize

            Timber.d("Loading from $page page $perPage items")
            val gamesCursor = storage.getGamesPage(
                query = query,
                key = page,
                count = perPage.toLong(),
            ).first()

            LoadResult.Page(
                data = gamesCursor.items,
                prevKey = null,
                nextKey = gamesCursor.nextKey
            )
        } catch (ex: Exception) {
            Timber.e(ex, "Local storage PagingSource loading error")
            LoadResult.Error(ex)
        }
    }
}