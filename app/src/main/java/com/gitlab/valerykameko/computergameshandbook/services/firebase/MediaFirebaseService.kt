package com.gitlab.valerykameko.computergameshandbook.services.firebase

import android.net.Uri
import com.google.firebase.storage.StorageException
import com.google.firebase.storage.StorageException.ERROR_OBJECT_NOT_FOUND
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.security.MessageDigest
import kotlin.random.Random

class MediaFirebaseService internal constructor(
    private val storageReference: StorageReference
) {
    companion object {
        private const val IMAGES_PATH = "image"
        private const val CLIPS_PATH = "clip"
        private const val MIN_HASH_LENGTH = 20
        private const val DEFAULT_HASH_ALGORITHM = "SHA-1"
        private val FILENAME_PREFIX_UNFOLD_RANGE = 0..2
        private val HASH_REGEX = Regex("([a-f0-9]{${MIN_HASH_LENGTH},})")
    }

    private val imagesReference = storageReference.child(IMAGES_PATH)
    private val clipsReference = storageReference.child(CLIPS_PATH)

    suspend fun getImageUri(uri: Uri): Uri? = withContext(Dispatchers.IO) {
        val imageUri = encodeImageUri(uri)
        val imageReference = getReference(imageUri)

        try {
            imageReference.downloadUrl.await()
        } catch (ex: StorageException) {
            if (ex.errorCode == ERROR_OBJECT_NOT_FOUND) {
                null
            } else {
                throw ex
            }
        }
    }

    suspend fun getImageData(uri: Uri): ByteArray? = withContext(Dispatchers.IO) {
        val imageUri = encodeImageUri(uri)
        val imageReference = getReference(imageUri)
        try {
            val imageStream = imageReference.stream.await()

            imageStream.stream.use {
                return@withContext it.readBytes()
            }
        } catch (ex: StorageException) {
            Timber.e(ex, "Image downloading error")
            null
        }
    }

    suspend fun addImage(data: ByteArray, extension: String): Uri = withContext(Dispatchers.IO) {
        val hsh = toHex(Random(MIN_HASH_LENGTH).nextBytes(20))

        val imageReference = imagesReference
            .child(hsh.substring(FILENAME_PREFIX_UNFOLD_RANGE))
            .child("${hsh}.${extension}")
        imageReference.putBytes(data).await()
        imageReference.downloadUrl.await().also {
            Timber.i("Created firebase image $it")
        }
    }

    suspend fun removeImage(uri: Uri) = withContext(Dispatchers.IO) {
        val imageUri = encodeImageUri(uri)
        val imageReference = getReference(imageUri)

        try {
            imageReference.delete().await()
        } catch (ex: StorageException) {
        }
    }

    suspend fun getClipUri(uri: Uri): Uri? = withContext(Dispatchers.IO) {
        val clipUri = encodeClipUri(uri)
        val clipReference = getReference(clipUri)

        try {
            clipReference.downloadUrl.await()
        } catch (ex: StorageException) {
            if (ex.errorCode == ERROR_OBJECT_NOT_FOUND) {
                null
            } else {
                throw ex
            }
        }
    }

    suspend fun addClip(data: ByteArray, extension: String): Uri = withContext(Dispatchers.IO) {
        val hsh = toHex(Random(MIN_HASH_LENGTH).nextBytes(20))

        val clipReference = clipsReference
            .child(hsh.substring(FILENAME_PREFIX_UNFOLD_RANGE))
            .child("${hsh}.${extension}")
        clipReference.putBytes(data).await()
        clipReference.downloadUrl.await()
    }

    suspend fun removeClip(uri: Uri) = withContext(Dispatchers.IO) {
        val clipUri = encodeClipUri(uri)
        val clipReference = getReference(clipUri)

        try {
            clipReference.delete().await()
        } catch (ex: StorageException) {
        }
    }

    suspend fun getClipData(uri: Uri): ByteArray? = withContext(Dispatchers.IO) {
        try {
            val clipUri = encodeClipUri(uri)
            val clipReference = getReference(clipUri)
            val clipStream = clipReference.stream.await()

            clipStream.stream.use {
                return@withContext it.readBytes()
            }
        } catch (ex: StorageException) {
            Timber.e(ex, "Clip downloading error")
            null
        }
    }

    private fun encodeImageUri(uri: Uri): Uri {
        val relativeUri = encodeRelativeUri(uri)
        return Uri.Builder()
            .appendPath(IMAGES_PATH)
            .appendEncodedPath(relativeUri.path)
            .build()
    }

    private fun encodeClipUri(uri: Uri): Uri {
        val relativeUri = encodeRelativeUri(uri)
        return Uri.Builder()
            .appendPath(CLIPS_PATH)
            .appendEncodedPath(relativeUri.path)
            .build()
    }

    private fun getReference(uri: Uri): StorageReference {
        val pathSegments = uri.pathSegments
        return pathSegments.fold(storageReference) { storageReference, segment ->
            storageReference.child(segment)
        }
    }

    private fun encodeRelativeUri(uri: Uri): Uri {
        val hash = extractHash(uri) ?: {
            val uriByteArray = uri.toString().toByteArray()
            val hashByteArray =
                MessageDigest.getInstance(DEFAULT_HASH_ALGORITHM).digest(uriByteArray)
            toHex(hashByteArray)
        }()
        return Uri.Builder()
            .appendPath(hash.substring(FILENAME_PREFIX_UNFOLD_RANGE))
            .appendPath("$hash.${extractExtension(uri)}")
            .build()
    }

    private fun extractHash(uri: Uri): String? {
        val hashes = HASH_REGEX.findAll(uri.path ?: "")
        val hash = hashes.firstOrNull()
        return hash?.value
    }

    private fun extractExtension(uri: Uri): String? =
        uri.lastPathSegment?.substringAfterLast('.', "")

    private fun toHex(bytes: ByteArray): String =
        bytes.joinToString(separator = "") { byte -> String.format("%02x", byte) }
}

