package com.gitlab.valerykameko.computergameshandbook

import android.app.Application
import timber.log.Timber

class ComputersHandbookApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}