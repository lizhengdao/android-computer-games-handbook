package com.gitlab.valerykameko.computergameshandbook.util

interface Translatable {
    val translation: Int
}