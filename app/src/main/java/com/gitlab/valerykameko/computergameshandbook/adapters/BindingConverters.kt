package com.gitlab.valerykameko.computergameshandbook.adapters

import androidx.databinding.InverseMethod

object BindingConverters {
    @JvmStatic
    fun convertStringToInt(string: String): Int? {
        return string.toIntOrNull()
    }

    @InverseMethod("convertStringToInt")
    @JvmStatic
    fun convertIntToString(value: Int?): String {
        return value?.toString() ?: ""
    }

    @JvmStatic
    fun convertStringToLong(string: String): Long? {
        return string.toLongOrNull()
    }

    @InverseMethod("convertStringToLong")
    @JvmStatic
    fun convertLongToString(value: Long?): String {
        return value?.toString() ?: ""
    }
}