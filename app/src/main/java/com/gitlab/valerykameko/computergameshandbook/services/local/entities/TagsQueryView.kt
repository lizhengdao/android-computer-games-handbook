package com.gitlab.valerykameko.computergameshandbook.services.local.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.TagQuery

data class TagsQueryView(
    val prefix: String,
    val count: Int,
    val ignoreCase: Boolean = true,
) {
    constructor(query: TagQuery) : this(
        prefix = query.prefix ?: "",
        count = query.count,
    )

    fun match(tagView: TagView) = when {
        !tagView.value.startsWith(prefix, ignoreCase) -> false
        else -> true
    }

}