package com.gitlab.valerykameko.computergameshandbook.services.local

import com.gitlab.valerykameko.computergameshandbook.services.local.entities.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

@ExperimentalCoroutinesApi
class PlatformsLocalService(
    private val gamesService: GamesLocalService
) {
    private val platforms: Flow<MutableSet<PlatformView>> = flow {
        val platforms = mutableSetOf<PlatformView>()
        gamesService.getGames().collect { chunk ->
            chunk.items.asSequence()
                .flatMap { it.platforms }
                .toCollection(platforms)
        }
        emit(platforms)
    }

    fun getPlatforms(query: PlatformsQueryView?): Flow<Chunk<PlatformView>> = platforms.map { tags ->
        val platformsSequence = tags.asSequence()
        val filteredPlatforms = query?.let { query ->
            platformsSequence.filter(query::match)
        } ?: platformsSequence

        val count = query?.count ?: filteredPlatforms.count()
        val platformsPrefix = filteredPlatforms
            .take(count)
            .toList()
        Chunk(
            count = platformsPrefix.size,
            items = platformsPrefix,
        )
    }
}