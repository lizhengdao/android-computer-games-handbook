package com.gitlab.valerykameko.computergameshandbook.data

import android.content.Context
import com.gitlab.valerykameko.computergameshandbook.data.model.Genre
import com.gitlab.valerykameko.computergameshandbook.data.model.GenreQuery
import com.gitlab.valerykameko.computergameshandbook.services.FirebaseServiceFactory
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.GenreView
import com.gitlab.valerykameko.computergameshandbook.services.firebase.entities.GenresQueryView
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber
import com.google.firebase.firestore.DocumentSnapshot

class GenresStorage(
    context: Context,
) {
    companion object {
        private const val MAX_TAGS_COUNT_SIZE = 50
    }

    private val genresFirebaseService by lazy { FirebaseServiceFactory.getGenresFirebaseService(context) }

    fun getGenres(query: GenreQuery?): Flow<List<Genre>> {
        Timber.d("Loading $query")
        return genresFirebaseService.getGenres(
            query = query?.copy(
                count = Integer.min(query.count, MAX_TAGS_COUNT_SIZE)
            )?.let(::GenresQueryView),
        ).map {  items ->
            Timber.d("Loaded $items")
            items.map(GenreView.Companion::fromView)
        }
    }
}

