package com.gitlab.valerykameko.computergameshandbook.services.local.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.Genre
import com.google.gson.TypeAdapter
import com.google.gson.annotations.JsonAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter

@JsonAdapter(GenreView.GenreJsonAdapter::class)
data class GenreView(
    val name: String,
) {
    class GenreJsonAdapter : TypeAdapter<GenreView>() {
        override fun write(out: JsonWriter, value: GenreView) {
            value.apply {
                out.value(name)
            }
        }

        override fun read(`in`: JsonReader): GenreView {
            val name = `in`.nextString()
            return GenreView(name)
        }
    }

    companion object {
        fun fromView(view: GenreView) = Genre(view.name)
        fun toView(genre: Genre) = GenreView(genre.name)
    }
}

