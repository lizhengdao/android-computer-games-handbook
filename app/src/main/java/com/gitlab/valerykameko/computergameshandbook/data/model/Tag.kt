package com.gitlab.valerykameko.computergameshandbook.data.model

data class Tag(
    val value: String,
)