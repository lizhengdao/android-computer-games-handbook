package com.gitlab.valerykameko.computergameshandbook.adapters

import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.data.model.GameField
import com.gitlab.valerykameko.computergameshandbook.data.model.Language
import com.gitlab.valerykameko.computergameshandbook.data.model.SortOrder

object GamesFilterBindingAdapters {
    @JvmStatic
    @BindingAdapter("gameFields")
    fun Spinner.bindGameFields(fields: List<GameField>?) {
        adapter = TranslatableArrayAdapter(context, R.layout.item_text_view, fields ?: listOf())
    }

    @JvmStatic
    @BindingAdapter("gameField", "gameFieldAttrChanged", requireAll = false)
    fun Spinner.bindGameField(field: GameField?, listener: InverseBindingListener) {
        val index = (adapter as TranslatableArrayAdapter).getIndex(field) ?: 0
        setSelection(index, true)

        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) = listener.onChange()
            override fun onNothingSelected(adapterView: AdapterView<*>) = listener.onChange()
        }
    }

    @JvmStatic
    @InverseBindingAdapter(attribute = "gameField")
    fun Spinner.inverseBindGameField(): GameField =
        selectedItem as GameField

    @JvmStatic
    @BindingAdapter("sortOrders")
    fun Spinner.bindSortOrders(sortOrders: List<SortOrder>?) {
        adapter = TranslatableArrayAdapter(context, R.layout.item_text_view, sortOrders ?: listOf())
    }

    @JvmStatic
    @BindingAdapter("sortOrder", "sortOrderAttrChanged", requireAll = false)
    fun Spinner.bindSortOrder(sortOrder: SortOrder?, listener: InverseBindingListener) {
        val index = (adapter as TranslatableArrayAdapter).getIndex(sortOrder) ?: 0
        setSelection(index)

        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) = listener.onChange()
            override fun onNothingSelected(adapterView: AdapterView<*>) = listener.onChange()
        }
    }

    @JvmStatic
    @InverseBindingAdapter(attribute = "sortOrder")
    fun Spinner.inverseBindSortOrder(): SortOrder =
        selectedItem as SortOrder

    @JvmStatic
    @BindingAdapter("language")
    fun TextView.bindLanguage(language: Language) {
        text = language.languageName
    }
}

