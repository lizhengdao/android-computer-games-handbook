package com.gitlab.valerykameko.computergameshandbook.views

import android.content.Context
import android.util.AttributeSet
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.data.model.Genre
import com.google.android.material.chip.Chip

class GenreChip(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = R.attr.chipStyle,
) : Chip(context, attrs, defStyle) {
    constructor(context: Context, genre: Genre) : this(context) {
        inflate(context, R.layout.item_genre_chip, null)
        text = genre.name
    }
}