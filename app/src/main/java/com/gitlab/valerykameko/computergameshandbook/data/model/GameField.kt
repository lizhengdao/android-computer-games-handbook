package com.gitlab.valerykameko.computergameshandbook.data.model

import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.util.Translatable

enum class GameField(
    val value: String?,
    override val translation: Int,
) : Translatable {
    NONE(null, R.string.game_field_none),
    TITLE("title", R.string.game_field_title),
    RATING("rating", R.string.game_field_rating),
    ADDED_BY("added_by", R.string.game_field_added_by),
}
