package com.gitlab.valerykameko.computergameshandbook.views

import android.content.Context
import android.util.AttributeSet
import com.gitlab.valerykameko.computergameshandbook.R
import com.gitlab.valerykameko.computergameshandbook.data.model.Tag
import com.google.android.material.chip.Chip

class TagChip(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = R.attr.chipStyle,
) : Chip(context, attrs, defStyle) {
    var tag: Tag? = null
    constructor(context: Context, tag: Tag) : this(context) {
        inflate(context, R.layout.item_tag_chip, null)
        text = tag.value
        this.tag = tag
    }
}