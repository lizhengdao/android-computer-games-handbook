package com.gitlab.valerykameko.computergameshandbook.services.firebase.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.TagQuery
import com.gitlab.valerykameko.computergameshandbook.services.firebase.whereStartsWith
import com.google.firebase.firestore.Query

data class TagsQueryView(
    val prefix: String,
    val count: Int,
    val ignoreCase: Boolean = true,
) {
    constructor(query: TagQuery) : this(
        prefix = query.prefix ?: "",
        count = query.count,
    )

    fun applyQuery(query: Query): Query {
        return query
            .whereStartsWith("name", prefix)
            .limit(count.toLong())
    }
}