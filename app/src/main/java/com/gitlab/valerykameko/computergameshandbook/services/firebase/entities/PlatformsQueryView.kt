package com.gitlab.valerykameko.computergameshandbook.services.firebase.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.PlatformQuery
import com.gitlab.valerykameko.computergameshandbook.services.firebase.whereStartsWith
import com.google.firebase.firestore.Query

data class PlatformsQueryView(
    val prefix: String,
    val count: Int,
    val ignoreCase: Boolean = true,
) {
    constructor(query: PlatformQuery) : this(
        prefix = query.prefix ?: "",
        count = query.count,
    )

    fun applyQuery(query: Query): Query {
        return query
            .whereStartsWith("name", prefix)
            .limit(count.toLong())
    }
}