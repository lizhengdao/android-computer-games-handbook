package com.gitlab.valerykameko.computergameshandbook.services.firebase.entities

import com.gitlab.valerykameko.computergameshandbook.data.model.FieldSorting
import com.gitlab.valerykameko.computergameshandbook.data.model.FieldsSorting
import com.gitlab.valerykameko.computergameshandbook.data.model.GameField
import com.gitlab.valerykameko.computergameshandbook.util.FieldSortingView
import com.gitlab.valerykameko.computergameshandbook.util.FieldsSortingView
import com.gitlab.valerykameko.computergameshandbook.util.SortingOrderView
import com.google.firebase.firestore.Query

class GameFieldSortingView(
    fieldSorting: FieldSorting<GameField>
) : FieldSortingView<GameFieldView, GameView>(
    field = GameFieldView.toView(
        fieldSorting.field),
    order = SortingOrderView.toView(fieldSorting.order),
) {
    override val comparator : Comparator<GameView> by lazy {
        val plainComparator = Comparator<GameView> { o1, o2 -> compareValuesBy(o1, o2, field::selectValue) }
        when (order) {
            SortingOrderView.ASC -> plainComparator
            SortingOrderView.DESC -> plainComparator.reversed()
        }
    }

    fun applyQuery(query: Query): Query {
        if (field == GameFieldView.NONE) {
            return query
        }
        return when (order) {
            SortingOrderView.ASC -> query.orderBy(field.name, Query.Direction.ASCENDING)
            SortingOrderView.DESC -> query.orderBy(field.name, Query.Direction.DESCENDING)
        }
    }
}

class GameFieldsSortingView(
    fieldsSorting: FieldsSorting<GameField>
) : FieldsSortingView<GameFieldView, GameView>(
    fieldsSorting = fieldsSorting.fieldsSorting.map(::GameFieldSortingView)
) {
    companion object {
        private val NO_ORDER_COMPARATOR = Comparator<GameView> { _, _ -> 0}
    }

    override val comparator : Comparator<GameView> by lazy {
        val comparators = this.fieldsSorting.map { it.comparator }
        comparators.fold(NO_ORDER_COMPARATOR) { acc, comparator ->
            acc.then(comparator)
        }
    }

    fun applyQuery(query: Query): Query {
        return fieldsSorting.fold(query) { resultQuery, fieldSortingView ->
            (fieldSortingView as GameFieldSortingView).applyQuery(resultQuery)
        }
    }
}