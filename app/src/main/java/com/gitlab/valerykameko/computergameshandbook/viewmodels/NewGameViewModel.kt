package com.gitlab.valerykameko.computergameshandbook.viewmodels

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.*
import com.gitlab.valerykameko.computergameshandbook.data.*
import com.gitlab.valerykameko.computergameshandbook.data.model.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

class NewGameViewModel(
    private val gamesStorage: GamesStorage,
    private val mediaStorage: MediaStorage,
    private val tagsStorage: TagsStorage,
    private val genresStorage: GenresStorage,
    private val platformsStorage: PlatformsStorage,
) : ViewModel() {
    companion object {
        private const val TAGS_COUNT = 10
        private const val PLATFORMS_COUNT = 10
        private const val GENRES_COUNT = 10
    }
    private val releasedDateFormat = SimpleDateFormat("MMM d, yyyy", Locale.getAvailableLocales()[0])

    private val _autocompleteTags = MutableLiveData(listOf<Tag>())
    private val _autocompleteGenres = MutableLiveData(listOf<Genre>())
    private val _autocompletePlatforms = MutableLiveData(listOf<Platform>())

    val title = MutableLiveData("")
    val rating = MutableLiveData(0.0f)
    val releasedAt = MutableLiveData<Date>()
    val releasedAtString = releasedAt.map { releasedDateFormat.format(it) }
    val bitmap = MutableLiveData<Bitmap?>(null)
    val addedCount = MutableLiveData<Long?>(0)
    val websiteUriString = MutableLiveData("")
    val websiteUri = MutableLiveData<Uri?>(null)
    val tags = MutableLiveData<List<Tag>>(listOf())
    val autocompleteTags: LiveData<List<Tag>> = _autocompleteTags
    val platforms = MutableLiveData<List<Platform>>(listOf())
    val autocompletePlatforms: LiveData<List<Platform>> = _autocompletePlatforms
    val genres = MutableLiveData<List<Genre>>(listOf())
    val autocompleteGenres: LiveData<List<Genre>> = _autocompleteGenres
    val description = MutableLiveData<String>("")

    suspend fun loadAutocompleteTags(prefix: String) {
        tagsStorage.getTags(
            TagQuery(prefix, TAGS_COUNT)
        ).collect { autocompleteTags ->
            _autocompleteTags.value = autocompleteTags
        }
    }

    suspend fun loadAutocompleteGenres(prefix: String) {
        genresStorage.getGenres(
            GenreQuery(prefix, GENRES_COUNT)
        ).collect { autocompleteGenres ->
            _autocompleteGenres.value = autocompleteGenres
        }
    }

    suspend fun loadAutocompletePlatforms(prefix: String) {
        platformsStorage.getPlatforms(
            PlatformQuery(prefix, PLATFORMS_COUNT)
        ).collect { autocompletePlatforms ->
            _autocompletePlatforms.value = autocompletePlatforms
        }
    }

    @ExperimentalCoroutinesApi
    suspend fun createGame(): String {
        val imageUri = bitmap.value?.let {
            val stream = ByteArrayOutputStream().apply {
                val bitmap = bitmap.value!!
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, this)
            }
            mediaStorage.addImage(stream.toByteArray(), "jpeg")
        }

        return gamesStorage.addGame(Game(
            id = "",
            name = title.value!!,
            rating = rating.value!!.toDouble(),
            releasedDate = releasedAt.value!!,
            addedCount = addedCount.value!!,
            platforms = platforms.value!!,
            tags = tags.value!!,
            genres = genres.value!!,
            description = description.value ?: "",
            websiteUri = websiteUri.value,
            imageUri = imageUri,
            clipUri = null,
        ))
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(
        private val gamesStorage: GamesStorage,
        private val mediaStorage: MediaStorage,
        private val tagsStorage: TagsStorage,
        private val genresStorage: GenresStorage,
        private val platformsStorage: PlatformsStorage,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            NewGameViewModel(gamesStorage, mediaStorage, tagsStorage, genresStorage, platformsStorage) as T
    }
}